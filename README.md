# Pawn Chess

This project consists of a simple pawn chess implementation done for the SEP WS19/20 at LMU Munich.  
 
Pawn Chess is a variation of chess played using only pawns. At the beginning of a game, all the pawns are placed on the base row.  
The white player begins, and then the players take turn making a move until one of the pawns gets to the opponent's baseline or no player can make a move.  
To begin, a player may move their pawn one or two spaces forward. After each player has made their first move is made, the players may only move their pawns one step diagonally forward.  

## Dependencies

The project requires Java 11.  
It is built with `gradle`, version 5.6.4. The provided `gradlew` wrapper automatically downloads and uses the correct gradle version.  
It was tested on Ubuntu 18.04.3 LTS and Mac OS Catalina 10.15.2.

## Running the Program

To run the program, download the jar file in the releases folder.  
In the console, from the file's path, run the following command:
```
java -jar pawn-chess.jar
```

## Game Play
The game can be played as hotseat game, single-player game, or network game.  
<img src="resource/images/pawn_chess_start_menu.png" alt="Start Menu" align="middle" width="400"/>  
A hotseat game allows you to play against another player on the same computer.  
In the single player game, you play against an AI-Algorithm.  
  
  
<img src="resource/images/pawn_chess_AI.gif" alt="Player against AI" align="middle" width="400"/>  
  
To play in Network mode against an opponent, the first player should connect as the server and the second as a client.   

### Further Developments

+ Useful error messages in the GUI for network game mode 
+ Significantly improve the UI
+ Unit testing  


