package chess;

import chess.model.Cell;
import chess.model.Chess;
import chess.model.GameField;
import chess.model.GameMode;
import chess.model.Pawn;
import chess.model.Phase;
import chess.model.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Shell {

  private static final char START_LETTER = 'A';
  private static final int START_ROW = 1;
  private Chess chess;

  private static final String GAME_PROMPT = "Chess> ";

  private static final String HOW_TO_PLAY = "How To Play: \n The white player begins, then the "
      + "players take turns making a move until one of their pawns reaches the opponent's"
      + "baseline.\n A pawn always moves in the direction of it's opponent's baseline. In"
      + "their first move, a pawn may move one or two steps forward; \n otherwise it may only"
      + "move one step forward. \n A pawn is only allowed to move diagonally in order to "
      + "capture an opponent's pawn.\n";
  private static final String COMMAND_LIST = "You can use the following commands: \n "
      + "NEW : Starts a new game. \n MOVE f t : Moves pawn from cell f to cell t."
      + "Example: Move A1 A2 \n QUIT: quits the application.\n "
      + "PRINT : Prints the chess board and the current player's turn.";
  /**
   * Read and process input until the quit command has been entered.
   *
   * @param args Command line arguments.
   * @throws IOException Error reading from stdin.
   */

  public static void main(String[] args) throws IOException {
    final Shell shell = new Shell();
    shell.run();
  }

  /**
   * Run the chess shell. Takes commands from the user and executes them.
   *
   * @throws IOException Thrown when failing to read from stdin.
   */
  private void run() throws IOException {
    BufferedReader in
        = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));

    boolean quit = false;
    while (!quit) {
      System.out.println(GAME_PROMPT);

      String command;
      if (null != (command = in.readLine())) {
        if (command.toUpperCase().startsWith("MOVE")) {
          doMove(command);
        } else {
          switch (command.toUpperCase().trim()) {
            case "QUIT":
              quit = true;
              break;
            case "PRINT":
              drawGameField();
              logCurrentPlayer();
              break;
            case "NEW HOTSEAT":
              chess = new Chess();
              break;
            case "HELP":
              System.out.println(HOW_TO_PLAY + COMMAND_LIST);
              break;
            default:
              System.out.println("Error! Unexpected use. "
                  + "Please type \"HELP\" to see how to use commands.");
              break;
          }
        }
      }
    }
  }

  private void drawGameField() {
    if (chess != null && chess.getState().getField() != null) {
      GameField gameField = chess.getState().getField();
      Pawn[][] field = chess.getState().getField().getField();
      String[] columns = {"A", "B", "C", "D", "E", "F", "G", "H"};
      //we go from top to bottom, starting with the highest row i.e row 8
      for (int i = field.length; i > 0; i--) {
        System.out.print(i + " ");
        int rowIndex = i - 1;
        for (int j = 0; j < field[rowIndex].length; j++) {
          Cell cell = new Cell(j, rowIndex);
          if (gameField.get(cell).isPresent()) {
            Pawn pawn = gameField.get(cell).get();
            String pawnFirstLetter
                = Character.toString(pawn.getPlayer().name().charAt(0)).toLowerCase();
            System.out.print(pawnFirstLetter);
          } else {
            System.out.print(".");
          }
        }
        System.out.print("\n");
      }
      //printing column names
      for (int i = 0; i < columns.length; i++) {
        if (i == 0) {
          System.out.print("  ");
        }
        System.out.print(columns[i]);
      }
      System.out.print("\n");
    }
  }

  private void logCurrentPlayer() {
    if (chess != null && chess.getState() != null) {
      System.out.println("Player's turn: " + chess.getState().getCurrentPlayer().toString());
    }
  }

  /**
   * Process the user input and attempt to make a move. checks if the given cell given is
   * well-formed i.e. if it has the correct syntax. The token must consist of exactly two letters,
   * where the former is a letter and the latter a number. If the move is successful, the
   * application additionally determines whether the player has won the game.
   *
   * @param command the user input that contains the source and target cell.
   */
  private void doMove(String command) {
    if (chess != null && chess.getState() != null
        && chess.getState().getCurrentPhase() == Phase.RUNNING) {
      Matcher matcher
          = Pattern.compile("^MOVE ([a-hA-H][1-8]) ([a-hA-H][1-8])$")
          .matcher(command.toUpperCase().trim());
      boolean hasMatched = matcher.find();
      if (!hasMatched) {
        System.out.println("Error! Unexpected use of command. Type HELP");
      } else {
        while (hasMatched) {
          if (matcher.groupCount() == 2) {
            String moveFrom = matcher.group(1).toUpperCase();
            String moveTo = matcher.group(2).toUpperCase();
            Cell fromCell = new Cell(parseColumnValue(moveFrom), parseRowValue(moveFrom));
            Cell toCell = new Cell(parseColumnValue(moveTo), parseRowValue(moveTo));
            boolean hasMoved = chess.move(fromCell, toCell);

            if (hasMoved) {
              //if move was executed, then the current player has been set to the opponent
              Player currentPlayer = chess.getState().getCurrentPlayer();
              logLastMove(currentPlayer, moveFrom, moveTo);
              if (chess.checkIfGameIsFinished()) {
                finishGame();
              } else if (chess.getState().getGameMode() == GameMode.HOTSEAT) {
                changePlayer();
              }
            } else {
              System.out.println("Error! Illegal move! ");
            }
            hasMatched = false;
          }
        }
      }

    }
  }

  private void changePlayer() {
    Player nextPlayer = Player.getOpponentOf(chess.getState().getCurrentPlayer());
    if (!chess.checkIfGameIsFinished() && chess.isNextMoveForPlayerPossible(nextPlayer)) {
      chess.getState().setCurrentPlayer(nextPlayer);
    } else {
      logPlayerMustMissedTurn();
    }
    logCurrentPlayer();
  }

  private void logPlayerMustMissedTurn() {
    if (chess.getState().getCurrentPlayer() == Player.WHITE) {
      System.out.println(Player.BLACK.toString() + " must miss a turn");
    } else {
      System.out.println(Player.WHITE.toString() + " must miss a turn");
    }
  }

  private void logLastMove(Player player, String from, String to) {
    System.out.println(player.toString() + " moved " + from.toString() + " to " + to.toString());
  }

  private void finishGame() {
    chess.getState().setCurrentPhase(Phase.FINISHED);
    if (chess.getState().getWinner().isPresent()) {
      Player winner = chess.getState().getWinner().get();
      logWinner(winner);
    } else {
      logUndecidedGame();
    }
  }

  private void logWinner(Player player) {
    System.out.println("Game over. " + player.toString() + " has won");
  }

  private void logUndecidedGame() {
    System.out.println("Game over. Draw!");
  }

  /**
   * Parse the row value of a given cell string and return the corresponding index from 0-7.
   *
   * <p>For example, <code>parseRowValue("C1") = 0</code>
   *
   * @param value the string to parse
   * @return the corresponding row index, from 0-7
   */
  private int parseRowValue(String value) {
    char number = value.charAt(1);
    if (!Character.isDigit(number)) {
      throw new IllegalArgumentException("Char '" + number + "' is not a number.");
    }
    return Character.getNumericValue(number) - START_ROW;
  }

  /**
   * Parse the column value of a given cell string and return the corresponding index from 0-7.
   *
   * <p>For example, <code>parseColumnValue("C1") = 2</code>
   *
   * @param value the string to parse
   * @return the corresponding row index, from 0-7
   */
  private int parseColumnValue(String value) {
    char letter = value.charAt(0);
    if (!Character.isLetter(letter)) {
      throw new IllegalArgumentException("Char '" + letter + "' is not a letter.");
    }
    return letter - START_LETTER;
  }
}
