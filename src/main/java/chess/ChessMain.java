package chess;

import chess.view.ChessController;
import chess.view.Controller;

import javax.swing.SwingUtilities;

/**
 * The main class of the chess application. Its only purpose is to set up the model-, view-,
 * and controller-objects.
 */
public class ChessMain {

  /**
   *  Invoke the actual starting method {@link ChessMain#showPawnGame()} on the <code>
   *  AWT event dispatching thread </code>. This causes the method to be executed asynchronously
   *  after all pending AWT events have been processed.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> showPawnGame());
  }

  /**
   * Initializes the main {@link Controller} class of this game, which is then responsible for
   * setting the game up such that a user can further interact with it.
   */
  private static void showPawnGame() {
    new ChessController().start();
  }
}
