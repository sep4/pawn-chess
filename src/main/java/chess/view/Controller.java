package chess.view;

import chess.model.Cell;

import java.io.IOException;
import java.net.InetAddress;

/**
 * The main controller interface of the chess game. It takes the actions from the user and handles
 * them accordingly. This is done by either invoking the necessary model-methods, or by directly
 * telling the view to change its graphical user-interface.
 */
public interface Controller {

  /**
   * Sets the view that the controller will use.
   *
   * @param view The {@link View}.
   */
  void setView(View view);

  /** Initializes and starts the graphical user interface. */
  void start();

  /** Sets the start screen up on which the user can select between different game modes. */
  void showStartView();

  /** Sets up a hotseat game that the user can afterwards play on. */
  void hotseatGameSelected();

  /** Sets up a single game that the user plays against an AI. */
  void singleGameSelected();

  /**
   * Sets up a client network game that allows the user to play against another player on the
   * network. Tries to connect to the server at the given address.
   *
   * @param serverAddress server address to connect to
   */
  void clientNetworkGameSelected(InetAddress serverAddress);

  /**
   * Sets up a server network game that allows the user to play against another player on the
   * network.
   */
  void serverNetworkGameSelected();

  /** Reset a game such that the game is in its initial state. */
  void resetGame() throws IOException;

  /**
   * Execute a step on the chess board.
   *
   * @param from The {@link Cell source cell}.
   * @param to The {@link Cell target cell}.
   * @return <code>true</code> if the move was executed successfully; <code>false</code> otherwise.
   */
  boolean move(Cell from, Cell to);

  /** Dispose any remaining resources. */
  void dispose();
}
