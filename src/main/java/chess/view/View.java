package chess.view;

import chess.model.Model;

/**
 * The main interface of the view. It gets the state it displays directly from the {@link Model}.
 */
public interface View {

  /** Show the graphical user interface of the chess game. */
  void showView();

  /** Shows the start menu to the user. */
  void showStartMenu();

  /** Shows the GameBoard to the user. */
  void showGame(Model model);

  /** Removes a game-view if there is currently shown one. */
  void removeGame();

  /**
   * Displays an error message to the user.
   *
   * @param message The message to be displayed
   */
  void showErrorMessage(String message);
}
