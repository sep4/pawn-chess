package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Model;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * The main-view class. Consists of a card-layout, such that depending on the user input the
 * requested frame can be simply exchanged by a new one.
 */
class BasicView extends JFrame implements View {

  private static final long serialVersionUID = 6320900067840491616L;

  private static final int MINIMUM_FRAME_HEIGHT = 400;
  private static final int MINIMUM_FRAME_WIDTH = 400;

  private static final String START_VIEW = "Start View";
  private static final String GAME_VIEW = "Game View";

  private final Controller controller;
  private final StartView startView;

  private ChessView chessView;

  private Container contentPane;
  private CardLayout cardLayout;

  BasicView(Controller controller) {
    super("Pawn Game");
    this.controller = requireNonNull(controller);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    startView = new StartView(controller);
    pack();

    initializeCardPanel();
  }

  private void initializeCardPanel() {
    setBackground(new Color(190, 188, 189));

    cardLayout = new CardLayout();
    setLayout(cardLayout);

    contentPane = getContentPane();
    contentPane.add(startView, START_VIEW);
  }

  @Override
  public void showView() {
    setLocationByPlatform(true);
    pack();
    setVisible(true);
  }

  @Override
  public void showStartMenu() {
    cardLayout.show(contentPane, START_VIEW);
    pack();
  }

  @Override
  public void showGame(Model model) {
    chessView = new ChessView(model, controller);
    contentPane.add(chessView, GAME_VIEW);

    cardLayout.show(getContentPane(), GAME_VIEW);
    pack();
  }

  @Override
  public void removeGame() {
    if (chessView != null) {
      contentPane.remove(chessView);
      chessView.dispose();
      chessView = null;
    }
  }

  @Override
  public void dispose() {
    removeGame();
    controller.dispose();
    super.dispose();
  }

  @Override
  public void showErrorMessage(String message) {
    if (chessView != null) {
      chessView.showErrorMessage(message);
    } else {
      JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}
