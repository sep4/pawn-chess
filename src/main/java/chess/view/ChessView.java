package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Model;
import chess.model.Phase;
import chess.model.Player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * The main view of the graphical user interface. It provides and connects all graphical elements
 * that are necessary for playing a chess game. Specifically, it allows two players to interactively
 * execute their moves, and allows the game to be reset at any particular time.
 */
public class ChessView extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = 1L;

  private static final int MINIMUM_FRAME_WIDTH = 400;
  private static final int MINIMUM_FRAME_HEIGHT = 400;

  private final DrawBoard drawBoard;

  private Model model;
  private Controller controller;

  private JPanel resetPanel;
  private JButton resetButton;
  private JButton quitGameButton;
  private JLabel infoLabel;
  private JLabel errorLabel;

  /**
   * Creates a view with all elements necessary for playing an interactive chess game.
   *
   * @param model The {@link Model} that handles the game logic.
   * @param controller The {@link Controller} that validates and forwards any input.
   */
  public ChessView(Model model, Controller controller) {
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    drawBoard = new DrawBoard(model, controller);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));

    initializeWidgets();
    createView();
    updatePhase();

    model.addPropertyChangeListener(this);
  }

  private void initializeWidgets() {
    int fontsize = 14;

    infoLabel = new JLabel();
    infoLabel.setForeground(Color.RED);
    infoLabel.setFont(new Font(infoLabel.getFont().getFontName(), Font.PLAIN, fontsize));

    resetPanel = new JPanel(new FlowLayout());

    quitGameButton = new JButton("QUIT");
    quitGameButton.addActionListener(new ChessViewListener());
    quitGameButton.setEnabled(true);

    resetButton = new JButton("RESET");
    resetButton.addActionListener(new ChessViewListener());
    resetButton.setEnabled(true);

    errorLabel = new JLabel();
    errorLabel.setForeground(Color.RED);
    errorLabel.setFont(new Font(errorLabel.getFont().getFontName(), Font.PLAIN, fontsize));

    updatePlayerTextInDisplay();
  }

  private void createView() {
    setLayout(new BorderLayout());
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(infoLabel);
    add(panel, BorderLayout.NORTH);

    add(drawBoard, BorderLayout.CENTER);

    resetPanel.add(quitGameButton);
    resetPanel.add(resetButton);
    resetPanel.add(errorLabel);
    add(resetPanel, BorderLayout.SOUTH);
  }

  private void updatePhase() {
    if (model.getState().getCurrentPhase() == Phase.RUNNING) {
      drawBoard.setVisible(true);
    } else {
      drawBoard.setVisible(false);
      setInfoLabelText("Waiting for another player to join the game...");
    }
  }

  private void updatePlayerTextInDisplay() {
    setInfoLabelText("Current Player: " + model.getState().getCurrentPlayer());
  }

  /**
   * Display a message in the view.
   *
   * @param message The message to be displayed.
   */
  private void setInfoLabelText(String message) {
    infoLabel.setText(message);
  }

  /**
   * Displays an error message to the user.
   *
   * @param message The message to be displayed
   */
  public void showErrorMessage(String message) {
    errorLabel.setText(message);
  }

  /** Hides an error message that was previously shown to the user. */
  private void hideErrorMessage() {
    errorLabel.setText("");
  }

  void dispose() {
    model.removePropertyChangeListener(this);
    drawBoard.dispose();
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(
        new Runnable() {

        @Override
        public void run() {
          handleChangeEvent(event);
        }
      });
  }

  /**
   * The observable (= model) has just published that it has changed its state. The GUI needs to be
   * updated accordingly here.
   *
   * @param event The event that has been fired by the model.
   */
  private void handleChangeEvent(PropertyChangeEvent event) {

    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      updatePlayerTextInDisplay();
      openGameIsOverDialog();
      hideErrorMessage();
      updatePhase();
    }
  }

  private void openGameIsOverDialog() {
    if (model.getState().getCurrentPhase() != Phase.FINISHED) {
      return;
    }

    Optional<Player> playerOpt = model.getState().getWinner();
    if (playerOpt.isPresent()) {
      Player p = playerOpt.get();
      setInfoLabelText("Player " + p + " has won. Click reset to start a new game.");
      showDialogWindow("Game Over!", p + " wins.");
    } else {
      setInfoLabelText("Game ended in a tie. Click reset to start a new game.");
      showDialogWindow("Game Over!", "It's a tie!");
    }
  }

  /**
   * Shows a message pane that displays the outcome of the game.
   *
   * @param header The header text.
   * @param message An elaborate message why the game has ended.
   */
  private void showDialogWindow(String header, String message) {
    Optional<ImageIcon> iconOpt = ResourceLoader.loadScaledImageIcon("images/award.png", 60, 80);
    JOptionPane.showMessageDialog(
        this,
        message,
        header,
        JOptionPane.INFORMATION_MESSAGE,
        iconOpt.isPresent() ? iconOpt.get() : null);
  }

  /**
   * A custom implementation of an {@link ActionListener} that is used for observing and forwarding
   * any actions made on the respective widgets of the {@link ChessView}.
   *
   */
  private class ChessViewListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      if (actionEvent.getSource() == resetButton) {
        handleResetButtonClick();
      } else if (actionEvent.getSource() == quitGameButton) {
        handleQuitGameButton();
      }
    }

    private void handleQuitGameButton() {
      int result =
          JOptionPane.showConfirmDialog(
          ChessView.this,
          "Do you really want to abort this game?",
          "Please confirm your choice",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE);

      //do nothing if user didn't click on the 'yes' option
      if (result == JOptionPane.YES_OPTION) {
        controller.showStartView();
      }
    }

    private void handleResetButtonClick() {
      int result =
          JOptionPane.showConfirmDialog(
          ChessView.this,
          "Do you really want to start a new game?",
          "Please confirm your choice",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE);

      //do nothing if user didn't click on the 'yes' option
      if (result == JOptionPane.YES_OPTION) {
        try {
          controller.resetGame();
        } catch (IOException e) {
          JOptionPane.showMessageDialog(
              null,
              "Creating game failed. The following error occurred: " + e.getMessage(),
              "Error creating game",
              JOptionPane.ERROR_MESSAGE);
        }
      }
    }
  }

}
