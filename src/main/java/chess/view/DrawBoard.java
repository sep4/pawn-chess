package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Cell;
import chess.model.GameField;
import chess.model.Model;
import chess.model.Pawn;
import chess.model.Player;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * A custom painting class that is responsible for drawing the complete chess board along with
 * the necessary pawns and their positions. That is, a proper display of a single chess field as
 * well as the available pawns in the game. Should a user click on a pawn, all possible moves are
 * shown.
 */
class DrawBoard extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = 1L;

  private final Model model;
  private final Controller controller;

  private Set<Cell> possibleMovesForSelectedCell = new HashSet<>();

  private int cellSize;

  private int horizontalOffset;
  private int verticalOffset;

  /**
   * Creates a DrawBoard that is used to draw the chess field. It requires a model to
   * allow for management of pawns on the board i.e retrieve their positions and possible moves,
   * and a controller for forwarding the user interactions done on this board.
   *
   * @param model      The model for getting necessary information about the pawns.
   * @param controller The controller that forwards the interactions of the user.
   */
  DrawBoard(Model model, Controller controller) {
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    model.addPropertyChangeListener(this);

    setBackground(new Color(100, 110, 110));
    configureActionListener();
  }

  private void configureActionListener() {
    addComponentListener(
        new ComponentAdapter() {

        @Override
        public void componentResized(ComponentEvent e) {
          repaint();
        }
      });

    addMouseListener(new DrawBoardListener());
  }

  /**
   * Shows the move hints for the cell on which a user has clicked. The DrawBoard
   * gets repainted afterwards.
   *
   * @param cell The cell that a user has clicked.
   */
  private void showMovingHints(Cell cell) {
    possibleMovesForSelectedCell = model.getPossibleMovesForPawn(cell);
    repaint();
  }

  /**
   * Removes all move hints of the pawns and repaints the board in case they were previously
   * shown.
   */
  private void removeMovingHints() {
    if (!possibleMovesForSelectedCell.isEmpty()) {
      possibleMovesForSelectedCell.clear();
      repaint();
    }
  }

  @Override
  protected void paintComponent(Graphics g) {

    Graphics2D g2D = (Graphics2D) g;
    // optional rendering options that are used for preference when drawing the image-icons
    g2D.setRenderingHint(
        RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

    //paint the background
    g2D.setColor(Color.DARK_GRAY);
    g2D.fillRect(0, 0, getWidth(), getHeight());

    //gap-size between each cell and its actual border
    int padding = 5;

    //the size of a single cell including its border
    //(simply resize the chess-window for a demo)
    cellSize =
      Math.min(
        ((getWidth() - padding) * 14 / 15 / GameField.SIZE),
        ((getHeight() - padding) * 9 / 10 / GameField.SIZE)
      );

    //remove the right/bottom border of all cells
    int cellWidth = cellSize - padding;
    int cellHeight = cellWidth;

    // compute the offset, i.e. the value at which the first cell is actually drawn
    horizontalOffset = (getWidth() - cellSize * GameField.SIZE + padding) / 2;
    verticalOffset = (getHeight() - cellSize * GameField.SIZE + padding) / 2;

    // column and row value ranging from 0 to 7
    int col = 0;
    int row = 0;

    int idx = horizontalOffset;
    int idy = verticalOffset;

    // flag used to decide the foreground-color of a cell (i.e., light or dark)
    boolean evenColumn = true;

    while (col < GameField.SIZE) {
      row = 0;
      idy = verticalOffset;
      boolean drawDarkCell = evenColumn;
      while (row < GameField.SIZE) {

        //draw the background of each cell, which is either light-colored or dark
        if (drawDarkCell) {
          g2D.setColor(Color.LIGHT_GRAY);
        } else {
          g2D.setColor(Color.WHITE);
        }
        g2D.drawRect(idx, idy, cellWidth, cellHeight);
        g2D.fillRect(idx, idy, cellWidth, cellHeight);

        Cell cell = new Cell(col, GameField.SIZE - row - 1);

        //highlights cell if a pawn is currently selected and this field is a possible
        //target cell
        if (possibleMovesForSelectedCell.contains(cell)) {
          g2D.setColor(Color.RED);
          highlightCell(g2D, idx, idy, cellWidth, cellHeight);
        }

        //draw a pawn on the cell if it is owned by the player
        Optional<Player> playerOpt = model.getState().getField().get(cell).map(Pawn::getPlayer);
        if (playerOpt.isPresent()) {
          drawPawn(playerOpt.get(), g2D, padding, idx, idy, cellWidth, cellHeight);
        }

        drawDarkCell = !drawDarkCell;
        idy = idy + cellHeight + padding;
        row++;
      }

      evenColumn = !evenColumn;
      idx = idx + cellWidth + padding;
      col++;
    }
  }

  /**
   * Draw all available moves for the current selected cell.
   *
   * @param g2d        The {@link Graphics2D} object that allows to draw on the board.
   * @param x          The coordinate marking the left point of the cell.
   * @param y          The coordinate marking the upper point of the cell.
   * @param cellWidth  The width of the cell.
   * @param cellHeight The height of the cell.
   */
  private void highlightCell(Graphics2D g2d, int x, int y, int cellWidth, int cellHeight) {
    Stroke defaultStroke = g2d.getStroke();

    // Graphics#drawRect draws only the border of the rectangle. The stroke-object is used
    // afterwards to adjust the thickness of the lines.
    g2d.setStroke(new BasicStroke(3));
    g2d.drawRect(x, y, cellWidth, cellHeight);

    // Set the stroke back to its original value, otherwise all further cell-lines would be drawn
    // with the same new thickness.
    g2d.setStroke(defaultStroke);
  }

  /**
   * Draw a pawn on the current selected cell.
   *
   * @param player     The player that owns the cell.
   * @param g          The {@link Graphics} object that allows to draw on the board.
   * @param padding    Used to determine the gap-size between the cell and its border
   * @param x          The coordinate marking the left point of the cell.
   * @param y          The coordinate marking the upper point of the cell.
   * @param cellWidth  The width of the cell.
   * @param cellHeight The height of the cell.
   */
  private void drawPawn(
      Player player, Graphics g, int padding, int x, int y, int cellWidth, int cellHeight) {
    Optional<Image> imgOpt = Optional.empty();

    switch (player) {
      case WHITE:
        g.setColor(Color.WHITE);
        imgOpt = ResourceLoader.WHITE_PAWN;
        break;
      case BLACK:
        g.setColor(Color.BLACK);
        imgOpt = ResourceLoader.BLACK_PAWN;
        break;
      default:
        throw new RuntimeException("Unhandled player: " + player);
    }

    if (imgOpt.isPresent()) {
      g.drawImage(
          imgOpt.get(),
          x + padding,
          y + padding,
          cellWidth - 2 * padding,
          cellHeight - 2 * padding,
          null);
    } else {
      //draw circles as fallback when loading the images fails
      g.fillOval(x + padding, y + padding, cellWidth - 2 * padding, cellHeight - 2 * padding);

      g.setColor(Color.BLACK);
      g.drawOval(x + padding, y + padding, cellWidth - 2 * padding, cellHeight - 2 * padding);
    }
  }

  /**
   * Check if the registered mouse-click event was made on the painted cells of the drawing board.
   *
   * <p>In this case an {@link Optional} with the actual {@link Cell} is returned.
   *
   * @param p the point of the registered mouse click event.
   * @return An {@link Optional} containing the x- and y-coordinate of the cell.
   */
  private Optional<Cell> screenToBoardCoords(Point p) {
    //check if registered click is on one of the drawn cells on the board
    if (p.x <= horizontalOffset || p.y <= verticalOffset
        || p.x >= getWidth() - horizontalOffset
        || p.y >= getHeight() - verticalOffset
        || cellSize == 0) {
      return Optional.empty();
    }

    //turn point into actual cell coordinate
    int column = (p.x - horizontalOffset) / cellSize;
    int row = (p.y - verticalOffset) / cellSize;

    return Optional.of(new Cell(column, GameField.SIZE - row - 1));
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(
        new Runnable() {
        @Override
        public void run() {
          handlePropertyChange(event);
        }
      });
  }

  /**
   * The model has just announced that it has changed its state. This method will repaint the
   * board such that the display will show the latest state.
   *
   * @param event The event that has been fired by the model.
   */
  private void handlePropertyChange(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      repaint();
    }
  }

  /**
   * Disposes the drawboard by unsubscribing it from the model.
   */
  void dispose() {
    model.removePropertyChangeListener(this);
  }

  /**
   * Overwrites the {@link MouseListener}-interface so that any clicks made by the user on the chess
   * chess board are registered and handled accordingly.
   */
  class DrawBoardListener extends MouseAdapter {

    // Optional containing the last clicked cell if the pawn on it belongs to the user
    private Optional<Cell> sourceCellOpt = Optional.empty();

    @Override
    public void mouseClicked(MouseEvent e) {
      if (e.getButton() == MouseEvent.BUTTON2) {
        //user middle-clicked --> Do nothing
        return;
      }

      if (e.getButton() == MouseEvent.BUTTON3) {
        //user right-clicked --> clear all possible moves if any
        disableMoveHints();
        return;
      }

      Optional<Cell> cellOpt = screenToBoardCoords(e.getPoint());
      if (cellOpt.isEmpty()) {
        //user clicked on the light gray area outside the blue background
        disableMoveHints();
        return;
      }

      Optional<Player> playerOpt =
          model.getState().getField().get(cellOpt.get()).map(Pawn::getPlayer);
      if (playerOpt.isPresent() && playerOpt.get() == model.getState().getCurrentPlayer()) {
        //the pawn of the clicked cell belongs to the current player
        //--> draw possible moves for selected pawn
        sourceCellOpt = cellOpt;
        showMovingHints(cellOpt.get());

      } else if (possibleMovesForSelectedCell.contains(cellOpt.get())) {
        //clicked cell is among possible moves
        //--> clear possible moves hints and pass request to the controller
        controller.move(sourceCellOpt.get(), cellOpt.get());
        disableMoveHints();

      } else {
        //player has clicked on a cell that doesn't belongn to them
        // --> clear moving hints
        disableMoveHints();
      }
    }

    private void disableMoveHints() {
      sourceCellOpt = Optional.empty();
      removeMovingHints();
    }
  }

}
