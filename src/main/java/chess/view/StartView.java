package chess.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * StartView of the game that provides buttons for choosing between the different game modes of the
 * game.
 */
class StartView extends JPanel {
  private static final long serialVersionUID = 1L;

  private static final int PREFERRED_FRAME_WIDTH = 500;
  private static final int PREFERRED_FRAME_HEIGHT = 500;

  private final Controller controller;

  /**
   * Creates a new starting view in which all required elements are setup accordingly.
   *
   * @param controller The controller used to forward any events registered by the buttons.
   */
  StartView(Controller controller) {
    this.controller = Objects.requireNonNull(controller);

    setPreferredSize(new Dimension(PREFERRED_FRAME_WIDTH, PREFERRED_FRAME_HEIGHT));

    createContent();
  }

  private void createContent() {
    JPanel panel = new JPanel();
    panel.setBackground(new Color(110, 110, 110));
    panel.setPreferredSize(new Dimension(500, 500));

    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0f;
    c.weighty = 1.0f;
    add(panel, c);

    panel.setLayout(new GridBagLayout());
    c = new GridBagConstraints();
    c.gridx = 0;
    c.ipadx = 30;
    c.ipady = 14;

    JButton hotseatGameButton =
        createButton("Hotseat Game", "Start an offline game", controller::hotseatGameSelected);
    panel.add(hotseatGameButton, c);

    JButton singleGameButton =
        createButton("Single Game", "Start a game against the ai", controller::singleGameSelected);
    panel.add(singleGameButton, c);

    JRadioButton clientButton = new JRadioButton("Client");

    JRadioButton serverButton = new JRadioButton("Server");
    serverButton.setEnabled(true);
    JTextField serverAddress = new JTextField("127.0.0.1", 14);

    JButton networkGameButton =
        createButton(
        "Network Game",
        "Start a network game",
          () -> {
            try {
              if (clientButton.isSelected()) {
                InetAddress address = InetAddress.getByName(serverAddress.getText());
                controller.clientNetworkGameSelected(address);
              } else {
                controller.serverNetworkGameSelected();
              }
            } catch (UnknownHostException e) {
                // may also be put in controller class; we put this here so that we can easily move
                // the focus on the server-address text field if the address is invalid
                JOptionPane.showMessageDialog(
                    null,
                    "Invalid server address: " + serverAddress.getText(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
              // let text field request focus after error was shown,
              // so that user can immediately fix the server address
              serverAddress.requestFocus();
            }
          });
    panel.add(networkGameButton, c);

    clientButton.setSelected(true);
    clientButton.setEnabled(true);
    c.anchor = GridBagConstraints.WEST;
    panel.add(clientButton, c);

    c.anchor = GridBagConstraints.EAST;
    c.gridy = 3;
    panel.add(serverButton, c);

    ButtonGroup clientOrServer = new ButtonGroup();
    clientOrServer.add(clientButton);
    clientOrServer.add(serverButton);

    Label serverAddressLabel = new Label("Server Address");
    serverAddressLabel.setEnabled(true);
    c.ipady = 1;
    c.anchor = GridBagConstraints.CENTER;
    c.gridy = 4;
    panel.add(serverAddressLabel, c);

    serverAddress.setEnabled(true);
    c.anchor = GridBagConstraints.CENTER;
    c.gridy = 5;
    panel.add(serverAddress, c);
  }

  private JButton createButton(String name, String tooltip, Runnable action) {
    JButton button = new JButton(name);
    button.setToolTipText(tooltip);
    button.setEnabled(true);
    button.setPreferredSize(new Dimension(150,50));
    button.setForeground(Color.BLACK);
    button.setBackground(Color.LIGHT_GRAY);
    button.addActionListener(actionEvent -> action.run());

    return button;
  }
}
