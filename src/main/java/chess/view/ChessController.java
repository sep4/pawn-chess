package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.AiChess;
import chess.model.Cell;
import chess.model.Chess;
import chess.model.GameField;
import chess.model.Model;
import chess.model.NetworkChess;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 * Implementation of the controller interface which handles the interaction between the main model-
 * and view-classes. Its main purpose is to check the validity of incoming requests and to handle
 * the requests accordingly.
 */
public class ChessController implements Controller {

  private Model model;
  private View view;

  private SwingWorker<Boolean, Void> moveWorker;

  /** Creates a controller object for a given model. */
  public ChessController() {
    view = new BasicView(this);
  }

  @Override
  public void setView(View view) {
    this.view = requireNonNull(view);
  }

  @Override
  public void start() {
    view.showStartMenu();
    view.showView();
  }

  @Override
  public void showStartView() {
    if (model != null) {
      leaveCurrentGame();
    }

    view.removeGame();
    view.showStartMenu();
  }

  private void leaveCurrentGame() {
    try {
      Objects.requireNonNull(model).stopGame();
    } catch (IOException e) {
      JOptionPane.showMessageDialog(
          null,
          "Leaving network game failed. The following error occurred: " + e.getMessage(),
          "Error leaving network game",
          JOptionPane.ERROR_MESSAGE);
    }
  }

  @Override
  public void hotseatGameSelected() {
    model = new Chess();
    view.showGame(model);
  }

  @Override
  public void singleGameSelected() {
    model = new AiChess();
    view.showGame(model);
  }

  @Override
  public void clientNetworkGameSelected(InetAddress serverAddress) {
    model = new NetworkChess(serverAddress);
    startNetworkGame();
  }

  @Override
  public void serverNetworkGameSelected() {
    model = new NetworkChess();
    startNetworkGame();
  }

  private void startNetworkGame() {
    try {
      model.newGame();
      view.showGame(model);

    } catch (IOException e) {
      JOptionPane.showMessageDialog(
          null,
          "Creating network game failed. The following error occurred: " + e.getMessage(),
          "Error creating network game",
          JOptionPane.ERROR_MESSAGE);
    }
  }

  @Override
  public void resetGame() throws IOException {
    leaveCurrentGame();
    model.newGame();
  }

  @Override
  public boolean move(Cell from, Cell to) {
    if (moveWorker != null) {
      showError("Move is already in progress");
      return false;
    }
    if (!GameField.isWithinBounds(from)) {
      showError("Source cell " + from + " is out of bounds.");
      return false;
    }
    if (!GameField.isWithinBounds(to)) {
      showError("Target cell " + to + " is out of bounds.");
      return false;
    }

    //SwingWorker is designed to be executed only once. Executing it more often will
    //result in invoking the doInBackground method for another time.
    //Due to this, we are using null references here to check whether there are
    // any pending moves.
    moveWorker =
      new SwingWorker<Boolean, Void>() {
        @Override
        protected Boolean doInBackground() throws Exception {
          return model.move(from, to);
        }

        @Override
        protected void done() {
          moveWorker = null;
          try {
            if (!get().booleanValue()) {
              showError("Failed to execute the move");
            }
          } catch (InterruptedException e) {
            // We should not have to wait in the first place
            throw new IllegalStateException(e.getMessage(), e);
          } catch (ExecutionException e) {
            throw new RuntimeException(e.getMessage(), e);
          } catch (CancellationException e) {
            // moveWorker got interrupted, probably intentionally by the user.
            // --> do nothing here
          }
        }
      };
    moveWorker.execute();

    return true;
  }

  private void showError(String s) {
    view.showErrorMessage(s);
  }

  @Override
  public void dispose() {
    if (moveWorker != null) {
      //cancel the pending move
      moveWorker.cancel(true);
    }
  }
}