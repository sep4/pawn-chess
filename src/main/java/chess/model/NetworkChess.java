package chess.model;

import chess.model.network.MatchMaking;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.io.IOException;

import java.net.InetAddress;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

/**
 * Model for playing pawn chess with another player over the network. Handles creation of server and
 * client. Each game between two players has its own server that only handles that game.
 *
 * @see MatchMaking
 */
public class NetworkChess implements Model, PropertyChangeListener {

  private MatchMaking matchMaking;

  /** Color of the local player. */
  private final Player assignedPlayer;
  /** Address of the server. May be null if local player hosts the server. */
  private final InetAddress address;
  /** Delegate model that handles the real game logic. */
  private Chess delegate = new Chess();


  /**
   * Creates a new network game that will host the server. This constructor does not automatically
   * start the network game, but {@link #newGame()} must be called explicitly to start the game.
   */
  public NetworkChess() {
    delegate.getState().setCurrentPhase(Phase.WAITING);
    assignedPlayer = Player.WHITE;
    address = null;
  }

  /**
   * Creates a new network game that connects to a server. This constructor does not automatically
   * start the network game, but {@link #newGame()} must be called explicitly to start the game.
   *
   * @param address the address of the server to connect to
   */
  public NetworkChess(InetAddress address) {
    delegate.getState().setCurrentPhase(Phase.WAITING);
    assignedPlayer = Player.BLACK;
    this.address = address;
  }


  @Override
  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    delegate.addPropertyChangeListener(pcl);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener pcl) {
    delegate.removePropertyChangeListener(pcl);
  }

  /**
   * Starts the network game. This method starts the client, and, if applicable, the server. If no
   * network address was given to the constructor of this object, a server and a client will be
   * created and the client will directly connect to this server. If a network address was given to
   * the constructor of this object, only a client will be created and directly try to connect to
   * the given network address.
   *
   * @throws IOException if an IOException occurs while starting the client or the server. May
   *     happen because a network address is already in use or not available.
   */
  @Override
  public void newGame() throws IOException {
    delegate.newGame();
    getState().setCurrentPhase(Phase.WAITING);
    matchMaking = new MatchMaking(this, address);
    matchMaking.start();
  }

  @Override
  public boolean move(Cell from, Cell to) {
    return delegate.move(from,to);
  }

  @Override
  public void undoMove() {
    delegate.undoMove();
  }

  @Override
  public void stopGame() throws IOException {
    delegate.stopGame();
    matchMaking.stop();
  }

  @Override
  public GameState getState() {
    return delegate.getState();
  }

  /** Sets the game state to the given value. Can be used to override the existing game state. */
  public void setState(GameState state) {
    delegate.setState(state);
  }

  @Override
  public Set<Cell> getPossibleMovesForPawn(Cell cell) {
    //Only return possible moves for the pawn if its actually is your pawn
    Optional<Pawn> maybePawn = getState().getField().get(cell);
    if (maybePawn.isPresent() && maybePawn.get().getPlayer() == assignedPlayer) {
      return delegate.getPossibleMovesForPawn(cell);
    } else {
      return Collections.emptySet();
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    if (matchMaking.isComplete() && getState().getCurrentPhase() == Phase.WAITING) {
      getState().setCurrentPhase(Phase.RUNNING);
      delegate.notifyListeners();
    }

    if (!matchMaking.isRunning()) {
      getState().setCurrentPhase(Phase.FINISHED);
      delegate.notifyListeners();
    }
  }
}
