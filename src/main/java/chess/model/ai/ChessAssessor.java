package chess.model.ai;

import chess.model.GameState;
import chess.model.Player;
import java.util.Arrays;
import java.util.List;

/**
 * A composite assessor class that allows to compute the score for a given {@link
 * GameState}-instance. The class consists of several other assessor classes that do the actual work
 * of rating the various parts in the state.
 *
 * @see PawnCountAssessor
 * @see DistanceCountAssessor
 * @see CapturingPawnAssessor
 * @see IsolatedPawnAssessor
 * @see WinVelocityAssessor
 */
class ChessAssessor implements StateAssessor {

  private static final List<StateAssessor> ASSESSORS =
      Arrays.asList(
          new PawnCountAssessor(), // computes the n value
          new DistanceCountAssessor(), // computes the d value
          new CapturingPawnAssessor(), // computes the c value
          new IsolatedPawnAssessor(), // computes the i value
          new WinVelocityAssessor() // computes the v value
          );

  @Override
  public double computeValue(GameState state, Player minPlayer, int depth) {
    double result = 0;
    for (StateAssessor assessor : ASSESSORS) {
      result += assessor.computeValue(state, minPlayer, depth);
    }
    return result;
  }
}
