package chess.model.ai;

import chess.model.Cell;
import chess.model.GameField;
import chess.model.GameState;
import chess.model.Player;
import java.util.HashSet;
import java.util.Set;

/**
 * Assessment class that evaluates the {@link GameState} by counting the amount of risked pawns on
 * the {@link GameField}. A pawn is at risk if it can be captured in the immediate next round by the
 * opponent player, without having a friendly pawn of the same color that is able to back it up.
 *
 * <p>The highest possible score can be achieved by {@link Player#WHITE} having as much pawns at
 * risk as possible, while at the same time {@link Player#BLACK} has as few pawns available that are
 * at risk.
 */
class CapturingPawnAssessor implements StateAssessor {

  @Override
  public double computeValue(GameState state, Player minPlayer, int depth) {
    int humanRisked = getRiskedPawnsForPlayer(state, minPlayer);

    int aiRisked = getRiskedPawnsForPlayer(state, Player.getOpponentOf(minPlayer));
    return humanRisked - MODIFIER * aiRisked;
  }

  private int getRiskedPawnsForPlayer(GameState state, Player player) {
    final int direction;
    switch (player) {
      case BLACK:
        direction = -1;
        break;
      case WHITE:
        direction = 1;
        break;
      default:
        throw new AssertionError("Unhandled player: " + player);
    }
    Set<Cell> playerCells = state.getAllCellsOfPlayer(player);
    Set<Cell> opponentCells = state.getAllCellsOfPlayer(Player.getOpponentOf(player));

    // first, filter all cells that have a supporting pawn
    Set<Cell> potentialCandidates = new HashSet<>();
    for (Cell cell : playerCells) {
      Cell supportingLeft = new Cell(cell.getColumn() - 1, cell.getRow() - direction);
      Cell supportingRight = new Cell(cell.getColumn() + 1, cell.getRow() - direction);
      if (!playerCells.contains(supportingLeft) && !playerCells.contains(supportingRight)) {
        // the cell is a candidate if it has neither a supporting cell on both the left *and* the
        // right
        potentialCandidates.add(cell);
      }
    }

    // finally, check which of the potential candidates are truly at risk
    int candiatesAtRisk = 0;
    for (Cell cell : potentialCandidates) {
      Cell opponentLeft = new Cell(cell.getColumn() - 1, cell.getRow() + direction);
      Cell opponentRight = new Cell(cell.getColumn() + 1, cell.getRow() + direction);

      if (opponentCells.contains(opponentLeft) || opponentCells.contains(opponentRight)) {
        candiatesAtRisk++;
      }
    }
    return candiatesAtRisk;
  }
}
