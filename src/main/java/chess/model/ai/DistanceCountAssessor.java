package chess.model.ai;

import chess.model.Cell;
import chess.model.GameField;
import chess.model.GameState;
import chess.model.Player;
import java.util.Set;

/**
 * Assessment class that rates the {@link GameState} by evaluating the distance of pawns in relation
 * to their baseline. The further the pawns have moved from their baseline, the higher is the
 * weighting of the score for the respective pawns.
 *
 * <p>The best possible score can be achieved by {@link Player#BLACK} having its pawns moved as far
 * towards the opponents baseline as possible, while at the same time {@link Player#WHITE} having
 * its pawns on a maximal possible distance towards the baseline of black.
 */
class DistanceCountAssessor implements StateAssessor {

  @Override
  public double computeValue(GameState state, Player minPlayer, int depth) {
    assert minPlayer == Player.WHITE; // minPlayer is the human player

    int humanPoints = getPointsOfWhitePlayer(state);
    int machinePoints = getPointsOfBlackPlayer(state);
    return machinePoints - MODIFIER * humanPoints;
  }

  private int getPointsOfWhitePlayer(GameState state) {
    Set<Cell> whiteCells = state.getAllCellsOfPlayer(Player.WHITE);
    return whiteCells.stream().mapToInt(Cell::getRow).reduce(0, Integer::sum);

    /* The code above is identical to the code below: */

    // int result = 0;
    // for (Cell cell : whiteCells) {
    //   result += cell.getRow();
    // }
    // return result;
  }

  private int getPointsOfBlackPlayer(GameState state) {
    Set<Cell> blackCells = state.getAllCellsOfPlayer(Player.BLACK);
    return blackCells
        .stream()
        .mapToInt(Cell::getRow)
        .map(x -> GameField.SIZE - 1 - x)
        .reduce(0, Integer::sum);
  }
}
