package chess.model.ai;

import static chess.model.Player.getOpponentOf;

import chess.model.Cell;
import chess.model.Chess;
import chess.model.GameState;
import chess.model.Model;
import chess.model.MoveStep;
import chess.model.Phase;
import chess.model.Player;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * An implementation of the minimax algorithm. It allows to compute a move for the pawn chess, in
 * which the potential loss in a worst-case-scenario is minimized. This means for the ai-player
 * ({@link Player#BLACK}) that it takes the best possible move out of all potential moves that the
 * opponent ({@link Player#WHITE} has forced upon the ai-player.
 */
public class MinimaxAlgorithm {

  private static final int LOOK_AHEAD = 3;

  private final StateAssessor assessor;

  private Model chess;
  private MoveStep savedMoveStep;
  private Player maxPlayer; // the AI player
  private Player minPlayer; // the human player

  /**
   * Creates a new instance of the {@link MinimaxAlgorithm}, in which the assessment of each {@link
   * GameState} is predefined by a given set of {@link ChessAssessor rules}.
   */
  public MinimaxAlgorithm() {
    assessor = new ChessAssessor();
  }

  /**
   * Computes a possible move for the current {@link GameState}, which is done by an underlying
   * minimax-algorithm. This method works only if the phase of the game is not yet set to {@link
   * Phase#FINISHED finished}.
   *
   * @param gameState The current {@link GameState} of the chess application.
   * @return A {@link MoveStep} that contains the computed source- and target-cell.
   */
  public MoveStep determineMove(GameState gameState) {
    if (gameState.getCurrentPhase() == Phase.FINISHED) {
      return null;
    }

    // create a new chess instance with a reference to this gameState,
    // so that we get access to methods like move(), undoMove(), etc.
    chess = new Chess(gameState);

    savedMoveStep = null;
    maxPlayer = gameState.getCurrentPlayer();
    minPlayer = getOpponentOf(maxPlayer);
    max(maxPlayer, 0);

    return savedMoveStep;
  }

  private double max(Player player, int currentDepth) {
    double currentStateScore = assessor.computeValue(chess.getState(), minPlayer, currentDepth);

    if (chess.getState().getCurrentPhase() == Phase.FINISHED || currentDepth >= LOOK_AHEAD) {
      return currentStateScore;
    }

    if (chess.getState().getCurrentPlayer() != player) {
      // player skipped
      return max(getOpponentOf(player), currentDepth + 1) + currentStateScore;
    }

    double maxValue = -Double.MAX_VALUE;
    MoveStep currentBestStep = null;

    Set<Cell> playerCells =
        createOrderedSetForCells(chess.getState().getAllCellsOfPlayer(player), player);

    for (Cell sourceCell : playerCells) {
      Set<Cell> targetCells =
          createOrderedSetForCells(chess.getPossibleMovesForPawn(sourceCell), player);

      for (Cell targetCell : targetCells) {
        chess.move(sourceCell, targetCell);

        double value = min(getOpponentOf(player), currentDepth + 1) + currentStateScore;

        chess.undoMove();

        if (Double.compare(value, maxValue) > 0) {
          maxValue = value;
          currentBestStep = new MoveStep(sourceCell, targetCell);
        }
      }
    }

    if (currentDepth == 0) {
      savedMoveStep = currentBestStep;
    }

    return maxValue;
  }

  private double min(Player player, int currentDepth) {
    double currentStateScore = assessor.computeValue(chess.getState(), minPlayer, currentDepth);

    if (chess.getState().getCurrentPhase() == Phase.FINISHED || currentDepth >= LOOK_AHEAD) {
      return currentStateScore;
    }

    if (chess.getState().getCurrentPlayer() != player) {
      // player skipped
      return min(getOpponentOf(player), currentDepth + 1) + currentStateScore;
    }

    double minValue = Double.MAX_VALUE;
    MoveStep currentBestStep = null;

    Set<Cell> playerCells =
        createOrderedSetForCells(chess.getState().getAllCellsOfPlayer(player), player);

    for (Cell sourceCell : playerCells) {
      Set<Cell> targetCells =
          createOrderedSetForCells(chess.getPossibleMovesForPawn(sourceCell), player);

      for (Cell targetCell : targetCells) {
        chess.move(sourceCell, targetCell);

        double value = max(getOpponentOf(player), currentDepth + 1) + currentStateScore;

        chess.undoMove();

        if (Double.compare(value, minValue) < 0) {
          minValue = value;
          currentBestStep = new MoveStep(sourceCell, targetCell);
        }
      }
    }

    if (currentDepth == 0) {
      savedMoveStep = currentBestStep;
    }

    return minValue;
  }

  private static Set<Cell> createOrderedSetForCells(Collection<Cell> cells, Player player) {
    Set<Cell> set = null;
    switch (player) {
      case BLACK:
        set = new TreeSet<>(Comparator.reverseOrder());
        break;
      case WHITE:
        set = new TreeSet<>(Comparator.naturalOrder());
        break;
      default:
        throw new AssertionError("Unhandled player: " + player);
    }

    set.addAll(cells);
    return set;
  }
}
