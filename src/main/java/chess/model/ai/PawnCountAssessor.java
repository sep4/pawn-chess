package chess.model.ai;

import chess.model.Cell;
import chess.model.GameState;
import chess.model.Player;
import java.util.Set;

/**
 * Assessment class that rates the {@link GameState} by evaluating the number of pawns on the {@link
 * GameField}.
 *
 * <p>In this implementation, the best possible score can be achieved by {@link Player#BLACK} having
 * as much pawns as possible on the field, while at the same time {@link Player#WHITE} is having as
 * few pawns as possible.
 */
class PawnCountAssessor implements StateAssessor {

  @Override
  public double computeValue(GameState state, Player minPlayer, int depth) {
    Set<Cell> whiteCells = state.getAllCellsOfPlayer(Player.WHITE); // human player
    Set<Cell> blackCells = state.getAllCellsOfPlayer(Player.BLACK); // ai player
    return blackCells.size() - MODIFIER * whiteCells.size();
  }
}
