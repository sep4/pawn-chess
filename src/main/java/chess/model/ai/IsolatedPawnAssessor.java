package chess.model.ai;

import chess.model.Cell;
import chess.model.GameField;
import chess.model.GameState;
import chess.model.Player;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Assessment class that rates the {@link GameState} by evaluating the amount of isolated pawns on
 * the {@link GameField}.
 *
 * <p>The highest possible score can be achieved by {@link Player#BLACK} having as few isolated
 * pawns as possible, while at the same time {@link Player#WHITE} having a maximal amount of
 * isolated pawns available.
 */
class IsolatedPawnAssessor implements StateAssessor {

  @Override
  public double computeValue(GameState state, Player minPlayer, int depth) {
    int isolatedHumanPawns = getIsolatedPawnCount(state, minPlayer);
    int isolatedMachinePawns = getIsolatedPawnCount(state, Player.BLACK);
    return isolatedHumanPawns - MODIFIER * isolatedMachinePawns;
  }

  private int getIsolatedPawnCount(GameState state, Player player) {
    int isolatedCount = 0;

    Set<Cell> playerCells = state.getAllCellsOfPlayer(player);
    Set<Cell> nonIsolatedCells = new HashSet<>();

    for (Cell cell : playerCells) {
      if (nonIsolatedCells.contains(cell)) {
        continue;
      }

      Collection<Cell> neighborCells = getNeighborOfCell(cell);
      neighborCells.retainAll(playerCells);
      if (!neighborCells.isEmpty()) {
        nonIsolatedCells.addAll(neighborCells);
      } else {
        isolatedCount++;
      }
    }
    return isolatedCount;
  }

  private List<Cell> getNeighborOfCell(Cell cell) {
    List<Cell> neighborCells = new ArrayList<>(8);

    for (int col = cell.getColumn() - 1; col <= cell.getColumn() + 1; col++) {
      for (int row = cell.getRow() - 1; row <= cell.getRow() + 1; row++) {
        if (col == cell.getColumn() && row == cell.getRow()) {
          continue;
        }

        Cell neighbor = new Cell(col, row);
        if (GameField.isWithinBounds(neighbor)) {
          neighborCells.add(neighbor);
        }
      }
    }

    return neighborCells;
  }
}
