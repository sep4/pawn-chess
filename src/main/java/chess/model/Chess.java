package chess.model;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;


/**
 * Implementation of a chess class that handles the logic for a chess game that consists of pawns
 * only. It is the entry-class to the internal structure of the game-model and provides all methods
 * necessary for an user-interface to playing a game successfully.
 */
public class Chess implements Model {

  private static final int DIRECTION_UPWARD = 1;
  private static final int DIRECTION_DOWNWARD = -1;

  private static final int EXPECTED_HISTORY_LENGTH = 60;

  private final PropertyChangeSupport support = new PropertyChangeSupport(this);
  private final Deque<GameState> stateHistory = new ArrayDeque<>(EXPECTED_HISTORY_LENGTH);

  private GameState state;

  /**
   * Initialize a new Chess-Game in which everything is set up in its initial position.
   */
  public Chess() {
    newGame();
  }

  /**
   * Creates a new chess object in which the passed {@link GameState}-object is taken as new
   * instance of this class.
   *
   * @param gameState A non-null {@link GameState}-object.
   */
  public Chess(GameState gameState) {
    state = requireNonNull(gameState);
  }

  @Override
  public synchronized void addPropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.addPropertyChangeListener(pcl);
  }

  @Override
  public synchronized void removePropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.removePropertyChangeListener(pcl);
  }

  /**
   * Invokes the firing of an event, such that any attached observer (i.e., {@link
   * PropertyChangeListener}) is notified that a change happened to this model.
   */
  protected void notifyListeners() {
    notifyListeners(true);
  }

  private void notifyListeners(boolean wasActiveChange) {
    support.firePropertyChange(STATE_CHANGED, null, this);
    if (wasActiveChange) {
      support.firePropertyChange(NEW_MOVE, null, this);
    }
  }

  @Override
  public void newGame() {
    this.state = new GameState(GameMode.HOTSEAT);
    initPawns(state.getField());

    notifyListeners();
  }

  private void initPawns(GameField gamefield) {
    int blackRow = getBaseRowIndexOfPlayer(Player.BLACK);
    int whiteRow = getBaseRowIndexOfPlayer(Player.WHITE);

    for (int column = 0; column < GameField.SIZE; column++) {
      // The cells of both players start in the same row respectively.
      // Only the column values change in each iteration.
      Cell blackCell = new Cell(column, blackRow);
      gamefield.set(blackCell, new Pawn(Player.BLACK));

      Cell whiteCell = new Cell(column, whiteRow);
      gamefield.set(whiteCell, new Pawn(Player.WHITE));
    }

  }

  /**
   * Returns the row-index in which the pawns of the respective player start.
   *
   * @param currentPlayer The player whose starting row is to be determined.
   * @return the row-index, ranging from 0 to 7.
   */
  private int getBaseRowIndexOfPlayer(Player currentPlayer) {
    switch (currentPlayer) {
      case BLACK:
        return GameField.SIZE - 1;
      case WHITE:
        return 0;
      default:
        throw new AssertionError("Error! Unhandled player: " + currentPlayer);
    }
  }

  @Override
  public synchronized GameState getState() {
    return state;
  }

  /**
   * Sets the game state to the given value. Can be used to override the existing game state. After
   * updating the game state, listeners will be notified about the change.
   *
   * @param state the new game state
   */
  synchronized void setState(GameState state) {
    this.state = state;
    notifyListeners(false);
  }


  @Override
  public boolean move(Cell from, Cell to) {
    boolean isPlayable = isPlayablePawn(from, to);

    if (!isPlayable) {
      return false;
    }

    Set<Cell> possibleMoves = getPossibleMovesForPawn(from);
    if (!possibleMoves.contains(to)) {
      return false;
    }

    // clone the state and record it
    stateHistory.push(state.makeCopy());

    Pawn movedPawn = state.getField().remove(from);
    state.getField().set(to, movedPawn);

    Player nextPlayer = Player.getOpponentOf(state.getCurrentPlayer());
    if (!checkIfGameIsFinished() && isNextMoveForPlayerPossible(nextPlayer)) {
      state.setCurrentPlayer(nextPlayer);
    }

    getState().increaseMoveCounter();

    notifyListeners();

    return true;
  }

  /**
   * Checks that the selected cell from which a move is to be made actually contains a pawn, and
   * that the pawn belongs to the current player.
   *
   * @param from the {@link Cell} containing a pawn that needs to be moved.
   * @param to the {@link Cell} to which the pawn is to be moved.
   */
  private boolean isPlayablePawn(Cell from, Cell to) {
    //make sure the from Cell has a Pawn
    if (state.getField().get(from).isEmpty()) {
      return false;
    }
    //Make sure the pawn to be moved belongs to the current player
    Pawn pawnToMove = state.getField().get(from).get();
    notifyListeners();

    return pawnToMove.getPlayer() == getState().getCurrentPlayer();
  }

  @Override
  public synchronized void undoMove() {
    state = stateHistory.pop();
    notifyListeners();
  }

  /**
   * Checks if a player can make any move using any of his pawns on the board.
   *
   * @param player The current player making a move.
   * @return <code>true</code> If the player has moves left and <code>false</code> if the player
   *          cannot any more moves.
   */
  public boolean isNextMoveForPlayerPossible(Player player) {
    Set<Cell> cells = state.getAllCellsOfPlayer(player);
    for (Cell cell : cells) {
      Set<Cell> possibleMoves = getPossibleMovesForPawn(player, cell);
      if (!possibleMoves.isEmpty()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if the game is over.
   * @return <code>true</code> if one player's pawn has reached the opponent's baseline or when no
   *     player can make a move. Otherwise it returns <code>false</code>.
   */

  public boolean checkIfGameIsFinished() {
    Player pawnReachedEnd = checkIfPawnReachedEnd();
    if (pawnReachedEnd != null) {
      getState().setWinner(pawnReachedEnd);
      getState().setCurrentPhase(Phase.FINISHED);

      return true;
    }
    if (!isNextMoveForPlayerPossible(Player.WHITE) && !isNextMoveForPlayerPossible(Player.BLACK)) {
      Player winner = countPawnsAndGetWinner();
      if (winner != null) {
        getState().setWinner(winner);
        getState().setCurrentPhase(Phase.FINISHED);

      }
      return true;
    }
    return false;
  }

  private Player checkIfPawnReachedEnd() {
    Pawn[][] field = state.getField().getField();
    Pawn[] finishingRowWhite = field[7];
    for (int i = 0; i < finishingRowWhite.length; i++) {
      Cell cell = new Cell(i, 7);
      if (state.getField().get(cell).isPresent()) {
        Pawn pawn = state.getField().get(cell).get();
        if (pawn.getPlayer() == Player.WHITE) {
          return pawn.getPlayer();
        }
      }
    }
    Pawn[] finishingRowBlack = field[0];
    for (int i = 0; i < finishingRowBlack.length; i++) {
      Cell cell = new Cell(i, 0);
      if (state.getField().get(cell).isPresent()) {
        Pawn pawn = state.getField().get(cell).get();
        if (pawn.getPlayer() == Player.BLACK) {
          return pawn.getPlayer();
        }
      }
    }
    return null;
  }

  private Player countPawnsAndGetWinner() {
    Pawn[][] field = state.getField().getField();
    int whitePawns = 0;
    int blackPawns = 0;
    for (int i = 0; i < field.length; i++) {
      for (int j = 0; j < field[i].length; j++) {
        Cell cell = new Cell(j, i);
        if (getState().getField().get(cell).isPresent()) {
          Pawn pawn = getState().getField().get(cell).get();
          if (pawn.getPlayer() == Player.WHITE) {
            whitePawns++;
          } else {
            blackPawns++;
          }
        }
      }
    }
    if (whitePawns > blackPawns) {
      return Player.WHITE;
    } else if (blackPawns > whitePawns) {
      return Player.BLACK;
    } else {
      return null;
    }
  }

  @Override
  public void stopGame() {
    state.setCurrentPhase(Phase.FINISHED);
    notifyListeners();
  }

  @Override
  public synchronized Set<Cell> getPossibleMovesForPawn(Cell cell) {
    return getPossibleMovesForPawn(state.getCurrentPlayer(), cell);
  }

  /**
   * Computes all possible moves for a current selected pawn. There are four possible moves in total
   * available for a single pawn, depending on the current position of the pawn as well as the
   * position of pawns from the opponent player.
   *
   * @param player The current {@link Player player}.
   * @param cell The {@link Cell cell} that the {@link Pawn pawn} is currently positioned.
   * @return A set of cells with all possible moves for the current selected pawn.
   */
  private Set<Cell> getPossibleMovesForPawn(Player player, Cell cell) {
    if (player != Player.WHITE && player != Player.BLACK) {
      throw new IllegalArgumentException("Unhandled player: " + player);
    }
    if (!state.getField().isCellOfPlayer(player, cell)) {
      throw new IllegalArgumentException(
        "Cell " + cell + " does not belong to player " + player);
    }

    GameField field = state.getField();
    int direction = player == Player.WHITE ? DIRECTION_UPWARD : DIRECTION_DOWNWARD;

    int sourceCol = cell.getColumn();
    int sourceRow = cell.getRow();

    //get the Pawn in the cell
    if (field.get(cell).isEmpty()) {
      return null;
    }

    Set<Cell> possibleMoves = new HashSet<>();

    //check immediate 2 upper (lower) neighbours of the same column
    Cell oneAhead = new Cell(sourceCol, sourceRow + direction);
    if (GameField.isWithinBounds(oneAhead) && field.get(oneAhead).isEmpty()) {
      possibleMoves.add(oneAhead);
    }

    Cell twoAhead = new Cell(sourceCol, sourceRow + 2 * direction);
    if (GameField.isWithinBounds(twoAhead)
        && field.get(twoAhead).isEmpty()
        && sourceRow == getBaseRowIndexOfPlayer(player)) {
      possibleMoves.add(twoAhead);
    }

    //check if capture is possible
    Player opponent = Player.getOpponentOf(player);
    Cell diagonalLeft = new Cell(sourceCol - direction, sourceRow + direction);
    if (GameField.isWithinBounds(diagonalLeft)
        && field.isCellOfPlayer(opponent, diagonalLeft)) {
      possibleMoves.add(diagonalLeft);
    }

    Cell diagonalRight = new Cell(sourceCol + direction, sourceRow + direction);
    if (GameField.isWithinBounds(diagonalRight)
        && field.isCellOfPlayer(opponent, diagonalRight)) {
      possibleMoves.add(diagonalRight);
    }

    return possibleMoves;
  }
}