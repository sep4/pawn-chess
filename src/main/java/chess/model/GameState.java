package chess.model;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of a data structure class that contains all necessary attributes in order to
 * successfully play a game.
 */
public class GameState implements Serializable {

  private Phase currentPhase;
  private Player currentPlayer;
  private GameField gameField;
  private Player winner;
  private GameMode gameMode;

  private int moveCounter;

  /**
   * Constructs a new <code>GameState</code>. The state begins in a clear state, which means that
   * 1.) no pawns are on the board initially, 2.) player white is set as the beginning player, and
   * 3.) the game has its {@link Phase} set to a running state.
   */
  GameState(GameMode gameMode) {
    this.gameMode = gameMode;
    currentPhase = Phase.RUNNING;
    currentPlayer = Player.WHITE;
    gameField = new GameField();
    winner = null;
    moveCounter = 0;
  }

  GameState(GameState state) {
    gameMode = state.gameMode;
    currentPhase = state.currentPhase;
    currentPlayer = state.currentPlayer;
    gameField = new GameField(state.gameField);
    winner = state.winner;
    moveCounter = state.moveCounter;
  }

  synchronized GameState makeCopy() {
    return new GameState(this);
  }

  /**
   * Return the current phase of the game.
   *
   * @return the current phase.
   */
  public synchronized Phase getCurrentPhase() {
    return currentPhase;
  }

  /**
   * Overwrites the current {@link Phase phase} of the game with the new one.
   *
   * @param currentPhase The new phase.
   */
  public synchronized void setCurrentPhase(Phase currentPhase) {
    this.currentPhase = requireNonNull(currentPhase);
  }

  /**
   * Returns the {@link GameField gamefield} that stores the data for each cell of the chess-board.
   *
   * @return the current gamefield.
   */
  public synchronized GameField getField() {
    return gameField;
  }

  /**
   * Return the player that is currently allowed to make a move.
   *
   * @return the current player
   */
  public synchronized Player getCurrentPlayer() {
    return currentPlayer;
  }

  /**
   * Set the active player. For example, if {@link Player#BLACK} was previously active, the new
   * player might be set to {@link Player#WHITE}, and vice versa.
   *
   * @param newPlayer The player that may make his move now.
   */
  public synchronized void setCurrentPlayer(Player newPlayer) {
    this.currentPlayer = newPlayer;
  }

  /**
   * Return the mode that the game is being played in.
   *
   * @return the game mode
   */
  public synchronized GameMode getGameMode() {
    return gameMode;
  }

  /**
   * Return the winner of the current game. This method may only be called if the current game is
   * finished.
   *
   * @return {@link Optional#empty()} if the game's a draw. Otherwise an optional that contains the
   *     winner
   */
  public synchronized Optional<Player> getWinner() {
    if (currentPhase != Phase.FINISHED) {
      throw new IllegalStateException(
        String.format(
          "Expected current phase to be %s, but instead it is %s",
          Phase.FINISHED, currentPhase));
    }
    return Optional.ofNullable(winner);
  }

  /**
   * Sets the owner of the pawn that has reached the opponent's base row as the winner of the game.
   *
   * @param winner the winner of the game
   */
  synchronized void setWinner(Player winner) {
    this.winner = winner;
  }

  /**
   * Return all {@link Cell cells} of the current chess board that belong to the requested player.
   *
   * @param player The player whose cells are to be retrieved.
   * @return A set of cells on which the player has currently his pawns upon.
   */
  public synchronized Set<Cell> getAllCellsOfPlayer(Player player) {
    requireNonNull(player);
    return gameField
      .getCellsOccupiedWithPawns()
      .entrySet()
      .stream()
      .filter(x -> player == x.getValue())
      .map(Map.Entry::getKey)
      .collect(Collectors.toCollection(HashSet::new));
  }

  void increaseMoveCounter() {
    moveCounter++;
  }

  public int getMoveCounter() {
    return moveCounter;
  }
}
