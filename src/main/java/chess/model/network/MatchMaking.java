package chess.model.network;

import chess.model.NetworkChess;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Class responsible for creating pawn games. Sets up network components, links them to the
 * corresponding game models and connects the network components to each other.
 */
public class MatchMaking {

  /** The game model that is linked to the network components. */
  private final NetworkChess chess;
  /** Address of the game server; may be null if localhost is server. */
  private InetAddress address;
  /** Server of the game; this is only non-null if localhost is the server. */
  private Server server;
  /** Client of this game instance. */
  private Client client;

  /**
   * Creates a new MatchMaking object. This does not yet start the network components, but {@link
   * #start()} must still be called after object creation.
   *
   * @param chess the game model to use
   * @param address the internet address to connect to. If <code>null</code>, a new server will be
   *     created upon calling {@link #start()}
   */
  public MatchMaking(NetworkChess chess, InetAddress address) {
    this.chess = chess;
    this.address = address;
  }

  /**
   * Starts the local network components to connect to a game.
   *
   * @throws IOException if server- or client-creation or a network connection fails
   */
  public void start() throws IOException {
    if (address == null) {
      address = InetAddress.getLocalHost();
      server = new Server();
      server.subscribe(chess);
      server.start();

      // sleep a short moment to give server thread time to start up
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        // ignore
      }
    }

    client = new Client(chess, address);
    client.subscribe(chess);
    client.start();
  }

  /**
   * Stop match making-process. Stops all involved network components.
   *
   * @throws IOException may occur on closing sockets
   */
  public void stop() throws IOException {
    if (server != null) {
      server.close();
    }

    if (client != null) {
      client.close();
    }
  }

  /** Returns whether the match is currently running. */
  public boolean isRunning() {
    return client != null && client.isRunning() && (server == null || server.isRunning());
  }

  /**
   * Returns whether match-making is complete. Match-making is complete if the local player is only
   * a client, or if it is a server and another player has successfully connected.
   */
  public boolean isComplete() {
    return server == null || server.getConnected() == 2;
  }
}
