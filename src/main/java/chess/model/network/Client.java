package chess.model.network;

import chess.model.Chess;
import chess.model.GameState;
import chess.model.NetworkChess;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

class Client implements NetworkModule, PropertyChangeListener {

  private InetAddress serverAddress;
  private Socket clientSocket;

  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
  private NetworkChess chess;
  private ObjectOutputStream toServer;

  Client(NetworkChess chess, InetAddress serverAddress) {
    this.serverAddress = serverAddress;
    this.chess = chess;
    chess.addPropertyChangeListener(this);
  }

  /**
   * Starts the client and connects to the server at the address given for this object's
   * construction.
   *
   * @throws IOException if no server is available at the given server address and port {@link
   *     NetworkModule#CHESS_PORT}
   */
  @Override
  public void start() throws IOException {
    clientSocket = new Socket(serverAddress, NetworkModule.CHESS_PORT);
    toServer = new ObjectOutputStream(clientSocket.getOutputStream());
    Thread receiverThread =
        new Thread(
            () -> {
              try (ObjectInputStream fromServer =
                  new ObjectInputStream(clientSocket.getInputStream())) {
                Object received = fromServer.readObject();
                while (received != null) {
                  GameState s = (GameState) received;
                  chess.setState(s);
                  received = fromServer.readObject();
                }
              } catch (IOException e) {
                // expected if server closes connection
                cleanUpConnection();
              } catch (ClassNotFoundException e) {
                throw new AssertionError(e);
              }
            });
    receiverThread.setDaemon(true);
    receiverThread.start();
  }

  @Override
  public boolean isRunning() {
    return clientSocket != null && !clientSocket.isClosed();
  }

  @Override
  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    if (propertyChangeEvent.getPropertyName().equals(Chess.NEW_MOVE)) {
      try {
        if (toServer != null) {
          // reset to flush cache
          toServer.reset();
          toServer.writeObject(chess.getState());
        }
      } catch (IOException e) {
        // expected if server closes connection
        cleanUpConnection();
      }
    }
  }

  private void notifyListeners() {
    pcs.firePropertyChange("Connections changed", null, null);
  }

  void subscribe(PropertyChangeListener pcl) {
    pcs.addPropertyChangeListener(pcl);
  }

  private void cleanUpConnection() {
    try {
      close();
      clientSocket = null;
      toServer = null;
    } catch (IOException e) {
      throw new AssertionError(e);
    }
    notifyListeners();
  }

  @Override
  public void close() throws IOException {
    if (clientSocket != null) {
      clientSocket.close();
    }
  }
}