package chess.model.network;

import java.io.Closeable;
import java.io.IOException;

public interface NetworkModule extends Closeable {
  /** Port that network modules are supposed to use. */
  int CHESS_PORT = 43200;

  /**
   * Starts the network module. This javadoc should be overridden by the implementing class to give
   * more information about the network module's start behavior.
   *
   * @throws IOException may happen for different reasons.
   */
  void start() throws IOException;

  /** Returns whether the module is currently running. */
  boolean isRunning();
}
