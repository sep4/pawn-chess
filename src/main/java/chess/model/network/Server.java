package chess.model.network;

import chess.model.GameState;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.ArrayList;
import java.util.List;

public class Server implements NetworkModule, Closeable {

  private static final int EXPECTED_CONNECTIONS = 2;
  private List<Socket> clientSockets;
  private List<ObjectOutputStream> clientOutputStreams;

  private int lastStepSent = -1;

  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
  private ServerSocket socket;

  Server() {
    clientSockets = new ArrayList<>(EXPECTED_CONNECTIONS);
    clientOutputStreams = new ArrayList<>(EXPECTED_CONNECTIONS);
  }

  /**
   * Starts the server and begins accepting client connections.
   *
   * @throws IOException if a server socket is already bound to {@link NetworkModule#CHESS_PORT}
   */
  @Override
  public void start() throws IOException {
    socket = new ServerSocket(NetworkModule.CHESS_PORT);
    Thread connectorThread = new Thread(() -> acceptConnections(socket));
    connectorThread.setDaemon(true);
    connectorThread.start();
  }

  private void acceptConnections(ServerSocket socket) {
    try {
      while (clientSockets.size() < EXPECTED_CONNECTIONS) {
        Socket newConnection = socket.accept();
        if (newConnection != null) {
          clientSockets.add(newConnection);
          clientOutputStreams.add(new ObjectOutputStream(newConnection.getOutputStream()));

          Thread receivingThread = new Thread(() -> receiveStates(newConnection));
          receivingThread.setDaemon(true);
          receivingThread.start();

          notifyListeners();
        }
      }
      // close socket after two connections are accepted so that
      // further connections are rejected.
      // If we just stop to read from the socket, further connections are
      // just ignored and new clients will not know that the server is unavailable
      socket.close();
    } catch (IOException e) {
      // may happen if socket is closed pre-maturely and is thus expected
    }
  }

  private void receiveStates(Socket newConnection) {
    try (InputStream fromClient = newConnection.getInputStream()) {
      ObjectInputStream statesFromClient = new ObjectInputStream(fromClient);
      Object received = statesFromClient.readObject();
      while (received != null) {
        GameState receivedState = (GameState) received;
        sendState(receivedState);
        received = statesFromClient.readObject();
      }
    } catch (IOException e) {
      // client connection broke off, stop full server
      cleanUpConnection();
    } catch (ClassNotFoundException e) {
      throw new AssertionError(e);
    }
  }

  private void sendState(GameState receivedState) {
    // small ArrayLists are actually more efficient than HashSets, so
    // we use that even though asymptotic runtime is worse for deletions
    if (receivedState.getMoveCounter() <= lastStepSent) {
      return;
    }

    for (ObjectOutputStream toClient : clientOutputStreams) {
      try {
        toClient.writeObject(receivedState);
      } catch (IOException e) {
        cleanUpConnection();
      }
    }
    lastStepSent = receivedState.getMoveCounter();
  }

  private void cleanUpConnection() {
    try {
      close();
    } catch (IOException e) {
      throw new AssertionError(e);
    }
    notifyListeners();
  }

  private void notifyListeners() {
    pcs.firePropertyChange("Connections changed", null, null);
  }

  int getConnected() {
    return clientSockets.size();
  }

  @Override
  public boolean isRunning() {
    return socket != null && !socket.isClosed();
  }

  void subscribe(PropertyChangeListener pcl) {
    pcs.addPropertyChangeListener(pcl);
  }

  @Override
  public void close() throws IOException {
    for (Socket clientSocket : clientSockets) {
      if (!clientSocket.isClosed()) {
        clientSocket.close();
      }
    }
    if (socket != null && !socket.isClosed()) {
      socket.close();
    }
  }
}
