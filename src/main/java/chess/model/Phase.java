package chess.model;

public enum Phase {
  WAITING,
  RUNNING,
  FINISHED,
}