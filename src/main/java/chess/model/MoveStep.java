package chess.model;

import java.util.Objects;

/**
 * A class that contains the information to do a single move-step in a pawn chess application. As
 * such it stores a source- and a target-{@link Cell}, which are both necessary for executing a
 * {@link Chess#move(Cell, Cell)}.
 */
public class MoveStep {

  private final Cell sourceCell;
  private final Cell targetCell;

  /**
   * Creates a new {@link MoveStep} for a given source- and target-cell.
   *
   * @param source A non-null {@link Cell}-object that marks the source location on a chess field.
   * @param target A non-null {@link Cell}-object that marks the target location on a chess field.
   */
  public MoveStep(Cell source, Cell target) {
    sourceCell = Objects.requireNonNull(source);
    targetCell = Objects.requireNonNull(target);
  }

  /**
   * Returns a {@link Cell} that marks the source location for a move.
   *
   * @return a {@link Cell} containing the source location.
   */
  public Cell getSourceCell() {
    return sourceCell;
  }

  /**
   * Returns a {@link Cell} that marks the target location for a move.
   *
   * @return a {@link Cell} containing the target location.
   */
  public Cell getTargetCell() {
    return targetCell;
  }

  @Override
  public String toString() {
    return sourceCell + " to " + targetCell;
  }
}