package chess.model;

public enum GameMode {
  HOTSEAT("Hotseat"),
  SINGLE("Single");

  private final String gameMode;

  GameMode(String gameMode) {
    this.gameMode = gameMode;
  }

  @Override
  public String toString() {
    return gameMode;
  }
}