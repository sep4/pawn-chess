package chess.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * A class whose sole responsibility is the management of pawns on the chess board. As such it
 * provides the data structure that allows to check and manipulate each entry on the board
 * accordingly.
 */

public class GameField implements Serializable {
  private static final long serialVersionUID = -5058205782537282603L;

  public static final int SIZE = 8;
  private Pawn[][] field;

  GameField() {
    field = new Pawn[SIZE][SIZE];
  }

  GameField(GameField gameField) {
    field = cloneField(gameField.field);;
  }

  private Pawn[][] cloneField(Pawn[][] field) {
    Pawn[][] clonedField = new Pawn[SIZE][SIZE];

    for (int i = 0; i < SIZE; i++) {
      clonedField[i] = field[i].clone();
    }
    return clonedField;
  }

  /**
   * Returns the representation of the pawns on the chess board.
   *
   * @return field a copy of the chess board as a 2D array
   */
  public Pawn[][] getField() {
    //To avoid exposing the internal representation of field, we return a copy
    return Arrays.copyOf(field, field.length);
  }

  /**
   * Return an {@link Optional} that may contain a {@link Pawn}, depending on if there is one
   * positioned on the cell.
   *
   * @param cell The cell to be checked.
   * @return An {@link Optional optional} containing the respective pawn in case of success.
   */
  public Optional<Pawn> get(Cell cell) {
    if (isWithinBounds(cell)) {
      return Optional.ofNullable(field[cell.getColumn()][cell.getRow()]);
    } else {
      throwErrorWhenOutOfBounds(cell);
    }
    return Optional.empty();
  }

  /**
   * Checks a {@link Cell cell} whether it is occupied by a pawn and in case of success, whether it
   * belongs to the respective {@link Player player}.
   *
   * @param player The player to be checked.
   * @param cell The cell that can contain a pawn.
   * @return <code>true</code> if the player has a pawn on the cell, <code>false</code> otherwise.
   */
  boolean isCellOfPlayer(Player player, Cell cell) {
    Optional<Pawn> pawnOpt = get(cell);
    return pawnOpt.isPresent() && pawnOpt.get().getPlayer() == player;
  }

  /**
   * Returns all {@link Cell cells} that are currently occupied by a pawn.
   *
   * @return A map with all cells that have a pawn on them.
   */
  public Map<Cell, Player> getCellsOccupiedWithPawns() {
    Map<Cell, Player> map = new HashMap<>();

    for (int column = 0; column < SIZE; column++) {
      for (int row = 0; row < SIZE; row++) {
        if (field[column][row] != null) {
          map.put(new Cell(column, row), field[column][row].getPlayer());
        }
      }
    }

    return map;
  }

  /**
   * Set a pawn on the given cell. Any pawns already on that cell will be overridden.
   *
   * @param cell cell to set pawn on
   * @param newValue new value (pawn) to set on the cell
   * @throws IllegalArgumentException if given cell is out of field bounds
   */
  void set(Cell cell, Pawn newValue) {
    if (isWithinBounds(cell)) {
      field[cell.getColumn()][cell.getRow()] = newValue;
    } else {
      throwErrorWhenOutOfBounds(cell);
    }
  }

  /**
   * Remove pawn from the given cell. This method only has to work if there is a pawn on the cell.
   *
   * @param cell cell to remove any pawn from
   * @return the pawn that was removed
   * @throws IllegalArgumentException if given cell is out of field bounds
   */
  Pawn remove(Cell cell) {
    if (isWithinBounds(cell)) {
      if (get(cell).isPresent()) {
        Pawn pawn = get(cell).get();
        field[cell.getColumn()][cell.getRow()] = null;
        return pawn;
      }
    } else {
      throwErrorWhenOutOfBounds(cell);
    }
    return null;
  }

  /**
   * Checks a cell for its bounds and throws an exception in case of failure.
   *
   * @param cell The cell to be checked.
   */
  private void throwErrorWhenOutOfBounds(Cell cell) {
    if (!isWithinBounds(cell)) {
      throw new IllegalArgumentException("Coordinates of cell are out of bounds: " + cell);
    }
  }

  /**
   * Checks a {@link Cell} if its column- and row-value is within the bounds. The valid range is
   * from 0 to 7 for each.
   *
   * @param cell The cell to be checked
   * @return <code>true</code> if within the bounds, <code>false</code> otherwise.
   */
  public static boolean isWithinBounds(Cell cell) {
    return cell.getColumn() >= 0
      && cell.getColumn() < SIZE
      && cell.getRow() >= 0
      && cell.getRow() < SIZE;
  }
}
